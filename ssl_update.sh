#!/usr/bin/env bash
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export PATH

v2ray_qr_config_file="/etc/v2ray/vmess_qr.json"
domain=$(grep '\"add\"' $v2ray_qr_config_file | awk -F '"' '{print $4}')

systemctl stop nginx &> /dev/null
sleep 1
"/root/.acme.sh"/acme.sh --cron --home "/root/.acme.sh" &> /dev/null
"/root/.acme.sh"/acme.sh --installcert -d ${domain} --fullchainpath /etc/nginx/ssl/v2ray.crt --keypath /etc/nginx/ssl/v2ray.key --ecc
sleep 1
systemctl start nginx &> /dev/null
